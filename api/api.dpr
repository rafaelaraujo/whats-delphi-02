program api;
{$APPTYPE GUI}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  View.Main in 'src\views\View.Main.pas' {Form1},
  Module.Web in 'src\modules\Module.Web.pas' {WebModule1: TWebModule},
  Data.Connection in 'src\datas\Data.Connection.pas' {Connection: TDataModule},
  Model.Answer in 'src\models\Model.Answer.pas',
  Model.Chat in 'src\models\Model.Chat.pas',
  Model.Contact in 'src\models\Model.Contact.pas',
  Model.Message in 'src\models\Model.Message.pas',
  Model.Question in 'src\models\Model.Question.pas',
  Provider.HTTPRequest in 'src\providers\Provider.HTTPRequest.pas',
  Provider.Message in 'src\providers\Provider.Message.pas',
  Provider.Messenger in 'src\providers\Provider.Messenger.pas',
  Provider.Reader in 'src\providers\Provider.Reader.pas',
  Service.Twilio in 'src\services\Service.Twilio.pas',
  Controller.Message in 'src\controllers\Controller.Message.pas';

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
