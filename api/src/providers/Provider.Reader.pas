unit Provider.Reader;

interface

uses
  System.SysUtils, System.Classes, System.NETEncoding, provider.message;

type
  TReader = class
  public
    class function StringToMessage(ABody: String): TMessage;
  end;

implementation


{ TReader }

class function TReader.StringToMessage(ABody: String): TMessage;
var
  i        : Integer;
  LMessage : TMessage;
  LBodyText: TStrings;
  LText    : String;
begin
  LText := StringReplace(ABody, '%0A', '%3Cbr%3E', [rfReplaceAll]);

  LBodyText := TStringList.Create;
  try
    LBodyText.Delimiter     := '&';
    LBodyText.DelimitedText := LText;
    LBodyText.Text          := TNetEncoding.URL.Decode(LBodyText.Text);

    LMessage := TMessage.Create;

    for i := 0 to LBodyText.Count - 1 do
    begin
      LMessage.SetProperty(LBodyText.Names[i], LBodyText.Values[LBodyText.Names[i]]);
    end;

    Result := LMessage;
  finally
    LBodyText.Free;
  end;
end;


end.
