unit Provider.HTTPRequest;

interface

uses
  System.SysUtils, System.Classes, System.Net.URLClient, System.Net.HttpClient, System.Net.HttpClientComponent;

type
  THTTPRequest = class
  strict private
    class var FUserName,
              FPassword,
              FBearer: String;

    class procedure FOnAuthEvent(const Sender: TObject; AnAuthTarget: TAuthTargetType; const ARealm, AURL: String;
                                 var AUserName, APassword: String; var AbortAuth: Boolean;
                                 var Persistence: TAuthPersistenceType);
  public
    class function Get(AURL: String; AResponse: TStream): IHTTPResponse; overload;
    class function Get(AURL: String; AResponse: TStream; AAuthUserName, AAuthPassword: String): IHTTPResponse; overload;
    class function Get(AURL: String; AResponse: TStream; ABearer: String): IHTTPResponse; overload;
    class function Post(AURL: String; ASource: TStream): IHTTPResponse; overload;
    class function Post(AURL: String; ASource: TStream; AAuthUserName, AAuthPassword: String): IHTTPResponse; overload;
    class function Put(AURL: String; ASource: TStream): IHTTPResponse; overload;
    class function Put(AURL: String; ASource: TStream; AAuthUserName, AAuthPassword: String): IHTTPResponse; overload;
  end;

implementation

{ THTTPRequest }

class function THTTPRequest.Get(AURL: String; AResponse: TStream; AAuthUserName, AAuthPassword: String): IHTTPResponse;
begin
  FUserName := AAuthUserName;
  FPassword := AAuthPassword;

  Result := Get(AURL, AResponse);
end;

class function THTTPRequest.Get(AURL: String; AResponse: TStream): IHTTPResponse;
var
  LHTTPClient: TNetHTTPClient;
  LHTTPRequest: TNetHTTPRequest;
  LHTTPHeader: TNetHeader;
  LHTTPHeaders: TNetHeaders;
begin
  LHTTPClient := TNetHTTPClient.Create(nil);

  if ((not FUserName.Trim.IsEmpty) or
      (not FPassword.Trim.IsEmpty)) then
  begin
    LHTTPClient.OnAuthEvent := FOnAuthEvent;
  end;

  if not FBearer.Trim.IsEmpty then
  begin
    LHTTPHeader := TNetHeader.Create('Authorization', 'Bearer ' + FBearer);
    LHTTPHeaders := LHTTPHeaders + [LHTTPHeader];
  end;

  try
    LHTTPClient.Accept      := 'application/json';
    LHTTPClient.ContentType := 'application/json';

    LHTTPRequest := TNetHTTPRequest.Create(nil);

    try
      LHTTPRequest.Client := LHTTPClient;

      with LHTTPRequest.Get(AURL, AResponse, LHTTPHeaders) do
      begin
        if not (StatusCode in [200, 201]) then
        begin
          raise Exception.Create(StatusText);
        end;
      end;
    finally
      FreeAndNil(LHTTPRequest);
    end;
  finally
    FreeAndNil(LHTTPClient);
  end;
end;

class procedure THTTPRequest.FOnAuthEvent(const Sender: TObject; AnAuthTarget: TAuthTargetType; const ARealm,
  AURL: String; var AUserName, APassword: String; var AbortAuth: Boolean; var Persistence: TAuthPersistenceType);
begin
  if not FBearer.Trim.IsEmpty then
  begin
    TNetHTTPClient(Sender).CustomHeaders['Authorization'] := 'Bearer ' + FBearer;
    Exit;
  end;

  AUserName := FUserName;
  APassWord := FPassword;
end;

class function THTTPRequest.Post(AURL: String; ASource: TStream): IHTTPResponse;
var
  LHTTPClient : TNetHTTPClient;
  LHTTPRequest: TNetHTTPRequest;
begin
  LHTTPClient := TNetHTTPClient.Create(nil);

  try
    if ((not FUserName.Trim.IsEmpty) or
        (not FPassword.Trim.IsEmpty) or
        (not FBearer.Trim.IsEmpty))then
    begin
      LHTTPClient.OnAuthEvent := FOnAuthEvent;
    end;

    LHTTPClient.Accept      := 'application/json';
    LHTTPClient.ContentType := 'application/json';

    LHTTPRequest := TNetHTTPRequest.Create(nil);

    try
      LHTTPRequest.Client := LHTTPClient;

      Result := LHTTPRequest.Post(AURL, ASource);

      if not (Result.StatusCode in [200, 201]) then
      begin
        raise Exception.Create(Result.StatusText);
      end;
    finally
      FreeAndNil(LHTTPRequest);
    end;
  finally
    FreeAndNil(LHTTPClient);
  end;
end;

class function THTTPRequest.Post(AURL: String; ASource: TStream; AAuthUserName, AAuthPassword: String): IHTTPResponse;
begin
  FUserName := AAuthUserName;
  FPassword := AAuthPassword;

  Result := Post(AURL, ASource);
end;

class function THTTPRequest.Put(AURL: String; ASource: TStream; AAuthUserName, AAuthPassword: String): IHTTPResponse;
begin
  FUserName := AAuthUserName;
  FPassword := AAuthPassword;

  Result := Put(AURL, ASource);
end;

class function THTTPRequest.Put(AURL: String; ASource: TStream): IHTTPResponse;
var
  LHTTPClient : TNetHTTPClient;
  LHTTPRequest: TNetHTTPRequest;
begin
  LHTTPClient := TNetHTTPClient.Create(nil);
  try
   if ((not FUserName.Trim.IsEmpty) or
       (not FPassword.Trim.IsEmpty) or
       (not FBearer.Trim.IsEmpty))then
    begin
      LHTTPClient.OnAuthEvent := FOnAuthEvent;
    end;

    LHTTPClient.Accept      := 'application/json';
    LHTTPClient.ContentType := 'application/x-www-form-urlencoded';

    LHTTPRequest := TNetHTTPRequest.Create(nil);
    try
      LHTTPRequest.Client := LHTTPClient;

      Result := LHTTPRequest.Put(AURL, ASource);

      if not (Result.StatusCode in [200, 201]) then
      begin
        raise Exception.Create(Result.StatusText);
      end;
    finally
      FreeAndNil(LHTTPRequest);
    end;
  finally
    FreeAndNil(LHTTPClient);
  end;
end;

class function THTTPRequest.Get(AURL: String; AResponse: TStream; ABearer: String): IHTTPResponse;
begin
  FBearer := ABearer;

  Result := Get(AURL, AResponse);

  FBearer := EmptyStr;
end;

end.
