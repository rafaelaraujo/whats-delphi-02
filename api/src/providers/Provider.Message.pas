unit Provider.Message;

interface

uses
  System.SysUtils, System.TypInfo, System.Rtti;

type
  TMessage = class
  private
    FTo: String;
    FMediaUrl: String;
    FBody: String;
    FFrom: String;
    procedure SetBody(const Value: String);
    procedure SetFrom(const Value: String);
    procedure SetMediaUrl(const Value: String);
    procedure SetTo(const Value: String);
  public
    property From: String read FFrom write SetFrom;
    property &To: String read FTo write SetTo;
    property Body: String read FBody write SetBody;
    property MediaUrl: String read FMediaUrl write SetMediaUrl;

    function SetProperty(AProperty, AValue: String): Boolean;
  end;

implementation

{ TMessage }

procedure TMessage.SetBody(const Value: String);
begin
  FBody := Value;
end;

procedure TMessage.SetFrom(const Value: String);
begin
  FFrom := Value;
end;

procedure TMessage.SetMediaUrl(const Value: String);
begin
  FMediaUrl := Value;
end;

function TMessage.SetProperty(AProperty, AValue: String): Boolean;
var
  LContext: TRttiContext;
  LType: TRttiType;
  LProperty: TRttiProperty;
  LInteger: Integer;
  LValue: TValue;
begin
  Result := False;

  LType := LContext.GetType(Self.ClassInfo);
  LProperty := LType.GetProperty(AProperty);

  if LProperty <> nil then
  begin
    Result := True;

    case LProperty.PropertyType.TypeKind of
      tkInteger, tkInt64:
        begin
          LProperty.SetValue(Self, StrToInt(AValue));
        end;

      tkEnumeration:
        begin
          LInteger := GetEnumValue(LProperty.PropertyType.Handle, AValue);
          TValue.Make(LInteger, LProperty.PropertyType.Handle, LValue);
          LProperty.SetValue(Self, LValue);
        end;

      tkFloat:
        begin
          LProperty.SetValue(Self, StrToFloat(AValue));
        end;

      tkString, tkChar, tkWChar, tkLString, tkWString, tkUString:
        begin
          LProperty.SetValue(Self, AValue);
        end;
    end;
  end;
end;


procedure TMessage.SetTo(const Value: String);
begin
  FTo := Value;
end;

end.
