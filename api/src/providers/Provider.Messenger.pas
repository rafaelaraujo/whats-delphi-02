unit Provider.Messenger;

interface

uses
   System.SysUtils, System.Classes, System.NETEncoding;

type
  TMessenger = class
    class procedure Send(AUserName, APassword, AFrom, ATo, ABody: String; AMediaURL: String = '');
  end;

implementation

{ TMessenger }

uses Service.Twilio;

class procedure TMessenger.Send(AUserName, APassword, AFrom, ATo, ABody, AMediaURL: String);
begin
  TThread.CreateAnonymousThread(
    procedure
    var
      LClient: TTwilioClient;
      LParams: TStrings;
    begin
      LClient := TTwilioClient.Create(AUserName, APassword);

      try
        LParams := TStringList.Create;

        try
          LParams.Clear;

          LParams.Add('From=' + AFrom);
          LParams.Add('To=' + ATo);
          LParams.Add('Body=' + ABody);

          if not AMediaURL.Trim.IsEmpty then
          begin
            LParams.Add('MediaUrl=' + AMediaURL);
          end;

          LClient.Post('Messages', LParams);
        finally
          LParams.Free;
        end;
      finally
        LClient.Free;
      end;
    end).Start;
end;

end.
