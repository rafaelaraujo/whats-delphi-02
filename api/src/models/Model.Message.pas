unit Model.Message;

interface

uses
  Data.Connection;

type
  TModelMessage = class
  private
    FTO: String;
    FBODY: String;
    FFROM: String;
    FCHAT_ID: Integer;
    FQUESTION_ID: Integer;
    procedure SetBODY(const Value: String);
    procedure SetCHAT_ID(const Value: Integer);
    procedure SetFROM(const Value: String);
    procedure SetTO(const Value: String);
    procedure SetQUESTION_ID(const Value: Integer);
  public
    property FROM: String read FFROM write SetFROM;
    property &TO: String read FTO write SetTO;
    property BODY: String read FBODY write SetBODY;
    property CHAT_ID: Integer read FCHAT_ID write SetCHAT_ID;
    property QUESTION_ID: Integer read FQUESTION_ID write SetQUESTION_ID;

    function Save(AConnection: TConnection): Boolean;

    class function CreateNew(AConnection: TConnection; AFrom, ATo, ABody: String; AChat_ID, AQuestion_ID: Integer): Boolean;
  end;

implementation

{ TModelMessage }

class function TModelMessage.CreateNew(AConnection: TConnection; AFrom, ATo, ABody: String;
  AChat_ID, AQuestion_ID: Integer): Boolean;
var
  LMessage: TModelMessage;
begin
  LMessage := TModelMessage.Create;
  try
    LMessage.FROM := Copy(AFrom, Pos(':', AFrom) + 1);
    LMessage.&TO := Copy(ATo, Pos(':', AFrom) + 1);
    LMessage.BODY := ABody;
    LMessage.CHAT_ID := AChat_ID;
    LMessage.QUESTION_ID := AQuestion_ID;

    Result := LMessage.Save(AConnection);
  finally
    LMessage.Free;
  end;
end;

function TModelMessage.Save(AConnection: TConnection): Boolean;
const
  INSERT = 'INSERT INTO "MESSAGE" ("FROM", "TO", BODY, CHAT_ID, QUESTION_ID) ' +
           'VALUES (:FROM, :TO, :BODY, :CHAT_ID, :QUESTION_ID)';
begin
  Result := AConnection.FDConn.ExecSQL(INSERT, [Self.FROM, Self.&TO, Self.BODY, Self.CHAT_ID, Self.QUESTION_ID]) > 0;
end;

procedure TModelMessage.SetBODY(const Value: String);
begin
  FBODY := Value;
end;

procedure TModelMessage.SetCHAT_ID(const Value: Integer);
begin
  FCHAT_ID := Value;
end;

procedure TModelMessage.SetFROM(const Value: String);
begin
  FFROM := Value;
end;

procedure TModelMessage.SetQUESTION_ID(const Value: Integer);
begin
  FQUESTION_ID := Value;
end;

procedure TModelMessage.SetTO(const Value: String);
begin
  FTO := Value;
end;

end.
