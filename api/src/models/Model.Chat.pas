unit Model.Chat;

interface

uses
  System.SysUtils, System.Classes, System.StrUtils, Data.DB, Data.Connection;

type
  TModelChat = class
  private
    FACTION_TYPE: String;
    FLAST: TDateTime;
    FQUESTION_ID: Integer;
    FFINISH: TDateTime;
    FID: Integer;
    FSTART: TDateTime;
    FCONTACT_ID: Integer;
    FSTOREDDATA: String;
    procedure SetACTION_TYPE(const Value: String);
    procedure SetCONTACT_ID(const Value: Integer);
    procedure SetFINISH(const Value: TDateTime);
    procedure SetID(const Value: Integer);
    procedure SetLAST(const Value: TDateTime);
    procedure SetQUESTION_ID(const Value: Integer);
    procedure SetSTART(const Value: TDateTime);
    procedure SetSTOREDDATA(const Value: String);

    function DoFind(AConnection: TConnection): Boolean;
    function DoCreate(AConnection: TConnection): Boolean;
    function DoSave(AConnection: TConnection): Boolean;
    procedure LoadProperties(ADataSet: TDataSet);
  public
    property ID: Integer read FID write SetID;
    property START: TDateTime read FSTART write SetSTART;
    property LAST: TDateTime read FLAST write SetLAST;
    property FINISH: TDateTime read FFINISH write SetFINISH;
    property ACTION_TYPE: String read FACTION_TYPE write SetACTION_TYPE;
    property STOREDDATA: String read FSTOREDDATA write SetSTOREDDATA;
    property QUESTION_ID: Integer read FQUESTION_ID write SetQUESTION_ID;
    property CONTACT_ID: Integer read FCONTACT_ID write SetCONTACT_ID;

    class function FindOrCreate(AConnection: TConnection; AContactID: Integer; out ACreated: Boolean): TModelChat;

    function GetStoredData(AData: String): String;
    procedure ClearStoredData(AConnection: TConnection);
    procedure SetQuestion(AConnection: TConnection; AQuestionID: Integer; AActionType: String = 'P'; ADataToStore: String = ''; AClearStoredData: Boolean = False);

    function Save(AConnection: TConnection): Boolean;
  end;

implementation

{ TModelChat }

procedure TModelChat.ClearStoredData(AConnection: TConnection);
const
  UPDATE = 'UPDATE CHAT SET STOREDDATA = NULL WHERE ID = :ID';
begin
  Self.STOREDDATA := EmptyStr;
  AConnection.FDConn.ExecSQL(UPDATE, [Self.ID], [ftInteger]);
end;

function TModelChat.DoCreate(AConnection: TConnection): Boolean;
const
  INSERT = 'INSERT INTO CHAT ("START", LAST, CONTACT_ID) VALUES (:START, :LAST, :CONTACT_ID)';
begin
  Result := AConnection.FDConn.ExecSQL(INSERT, [Now, Now, Self.CONTACT_ID], [ftDateTime, ftDateTime, ftInteger]) > 0;
end;

function TModelChat.DoFind(AConnection: TConnection): Boolean;
const
  FIND_BY_CONTACT_ID = 'SELECT * FROM CHAT WHERE CONTACT_ID = %d AND FINISH IS NULL';

var
  LDataSet: TDataSet;
begin
  Result := False;

  AConnection.FDConn.ExecSQL(Format(FIND_BY_CONTACT_ID, [Self.CONTACT_ID]), LDataSet);
  try
    if not LDataSet.IsEmpty then
    begin
      LoadProperties(LDataSet);
      Result := True;
    end;
  finally
    LDataSet.Free;
  end;
end;

function TModelChat.DoSave(AConnection: TConnection): Boolean;
const
  UPDATE_CHAT = 'UPDATE CHAT SET LAST = :LAST, ' +
                                'ACTION_TYPE = :ACTION_TYPE, ' +
                                'STOREDDATA = :STOREDDATA, ' +
                                'QUESTION_ID = :QUESTION_ID ' +
                'WHERE ID = :ID';
begin
  Result := AConnection.FDConn.ExecSQL(UPDATE_CHAT,
                                       [Now, Self.ACTION_TYPE, Self.STOREDDATA, Self.QUESTION_ID, Self.ID],
                                       [ftDateTime, ftString, ftString, ftInteger, ftInteger]) > 0;
end;

class function TModelChat.FindOrCreate(AConnection: TConnection; AContactID: Integer; out ACreated: Boolean): TModelChat;
var
  LChat: TModelChat;
begin
  ACreated := False;

  LChat := TModelChat.Create;

  LChat.CONTACT_ID := AContactID;

  if not LChat.DoFind(AConnection) then
  begin
    LChat.DoCreate(AConnection);
    LChat.DoFind(AConnection);
    ACreated := True;
  end;

  Result := LChat;
end;

function TModelChat.GetStoredData(AData: String): String;
var
  LData: TStrings;
begin
  LData := TStringList.Create;
  try
    LData.Delimiter := '&';
    LData.DelimitedText := Self.STOREDDATA;

    Result := LData.Values[AData];
  finally
    LData.Free;
  end;
end;

procedure TModelChat.LoadProperties(ADataSet: TDataSet);
begin
  ID := ADataSet.FieldByName('ID').AsInteger;
  START := ADataSet.FieldByName('START').AsDateTime;
  LAST := ADataSet.FieldByName('LAST').AsDateTime;
  FINISH := ADataSet.FieldByName('FINISH').AsDateTime;
  STOREDDATA := ADataSet.FieldByName('STOREDDATA').AsString;
  ACTION_TYPE := ADataSet.FieldByName('ACTION_TYPE').AsString;
  QUESTION_ID := ADataSet.FieldByName('QUESTION_ID').AsInteger;
  CONTACT_ID := ADataSet.FieldByName('CONTACT_ID').AsInteger;
end;

function TModelChat.Save(AConnection: TConnection): Boolean;
const
  UPDATE = 'UPDATE CHAT SET FINISH = :FINISH WHERE ID = :ID';
begin
  Result := AConnection.FDConn.ExecSQL(UPDATE, [Self.FINISH, Self.ID], [ftDateTime, ftInteger]) > 0;
end;

procedure TModelChat.SetACTION_TYPE(const Value: String);
begin
  FACTION_TYPE := Value;
end;

procedure TModelChat.SetCONTACT_ID(const Value: Integer);
begin
  FCONTACT_ID := Value;
end;

procedure TModelChat.SetFINISH(const Value: TDateTime);
begin
  FFINISH := Value;
end;

procedure TModelChat.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TModelChat.SetLAST(const Value: TDateTime);
begin
  FLAST := Value;
end;

procedure TModelChat.SetQuestion(AConnection: TConnection; AQuestionID: Integer; AActionType, ADataToStore: String;
  AClearStoredData: Boolean);
begin
  Self.QUESTION_ID := AQuestionID;
  Self.ACTION_TYPE := AActionType;

  if AClearStoredData then
  begin
    Self.STOREDDATA := EmptyStr;
  end;

  if not ADataToStore.Trim.IsEmpty then
  begin
    Self.STOREDDATA := IfThen(not Self.STOREDDATA.Trim.IsEmpty, Self.STOREDDATA + '&') + ADataToStore;
  end;

  DoSave(AConnection);
end;

procedure TModelChat.SetQUESTION_ID(const Value: Integer);
begin
  FQUESTION_ID := Value;
end;

procedure TModelChat.SetSTART(const Value: TDateTime);
begin
  FSTART := Value;
end;

procedure TModelChat.SetSTOREDDATA(const Value: String);
begin
  FSTOREDDATA := Value;
end;

end.
