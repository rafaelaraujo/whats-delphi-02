unit Model.Contact;

interface

uses
  System.SysUtils, Data.DB, Data.Connection, System.Generics.Collections;

type
  TModelContact = class
  private
    FNAME: String;
    FID: Integer;
    FPHONE_NUMBER: String;
    procedure SetID(const Value: Integer);
    procedure SetNAME(const Value: String);
    procedure SetPHONE_NUMBER(const Value: String);

    function DoFind(AConnection: TConnection): Boolean;
    function DoCreate(AConnection: TConnection): Boolean;
    procedure LoadProperties(ADataSet: TDataSet);
  public
    property ID: Integer read FID write SetID;
    property NAME: String read FNAME write SetNAME;
    property PHONE_NUMBER: String read FPHONE_NUMBER write SetPHONE_NUMBER;

    class function FindOrCreate(AConnection: TConnection; APhoneNumber: String; out ACreated: Boolean): TModelContact;
    class function GetAll(AConnection: TConnection): TObjectList<TModelContact>;

    function Save(AConnection: TConnection): Boolean;
  end;

implementation

{ TModelContact }

function TModelContact.DoCreate(AConnection: TConnection): Boolean;
const
  INSERT = 'INSERT INTO CONTACT (NAME, PHONE_NUMBER) VALUES (:NAME, :PHONE_NUMBER)';
begin
  Result := AConnection.FDConn.ExecSQL(INSERT, [Self.NAME, Self.PHONE_NUMBER], [ftString, ftString]) > 0;
end;

function TModelContact.DoFind(AConnection: TConnection): Boolean;
const
  FIND_BY_PHONE = 'SELECT * FROM CONTACT WHERE PHONE_NUMBER = ''%s''';

var
  LDataSet: TDataSet;
begin
  Result := False;

  AConnection.FDConn.ExecSQL(Format(FIND_BY_PHONE, [Self.PHONE_NUMBER]), LDataSet);
  try
    if not LDataSet.IsEmpty then
    begin
      LoadProperties(LDataSet);
      Result := True;
    end;
  finally
    LDataSet.Free;
  end;
end;

class function TModelContact.FindOrCreate(AConnection: TConnection; APhoneNumber: String;
  out ACreated: Boolean): TModelContact;
var
  LPhoneNumber: String;
  LContact: TModelContact;
begin
  ACreated := False;

  LPhoneNumber := Copy(APhoneNumber, Pos(':', APhoneNumber) + 1);

  LContact := TModelContact.Create;

  LContact.PHONE_NUMBER := LPhoneNumber;

  if not LContact.DoFind(AConnection) then
  begin
    LContact.DoCreate(AConnection);
    LContact.DoFind(AConnection);
    ACreated := True;
  end;

  Result := LContact;
end;

class function TModelContact.GetAll(AConnection: TConnection): TObjectList<TModelContact>;
const
  SELECT = 'SELECT * FROM CONTACT ORDER BY ID';

var
  LContactList: TObjectList<TModelContact>;
  LContact: TModelContact;
  LDataSet: TDataSet;
begin
  LContactList := TObjectList<TModelContact>.Create;

  AConnection.FDConn.ExecSQL(SELECT, LDataSet);
  try
    while not LDataSet.Eof do
    begin
      LContact := TModelContact.Create;

      LContact.LoadProperties(LDataSet);

      LContactList.Add(LContact);

      LDataSet.Next
    end;
  finally
    LDataSet.Free;
  end;

  Result := LContactList;
end;

procedure TModelContact.LoadProperties(ADataSet: TDataSet);
begin
  Self.ID := ADataSet.FieldByName('ID').AsInteger;
  Self.NAME := ADataSet.FieldByName('NAME').AsString;
  Self.PHONE_NUMBER := ADataSet.FieldByName('PHONE_NUMBER').AsString;
end;

function TModelContact.Save(AConnection: TConnection): Boolean;
const
  UPDATE = 'UPDATE CONTACT SET NAME = :NAME WHERE ID = :ID';
begin
  Result := AConnection.FDConn.ExecSQL(UPDATE, [Self.NAME, Self.ID], [ftString, ftInteger]) > 0;
end;

procedure TModelContact.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TModelContact.SetNAME(const Value: String);
begin
  FNAME := Value;
end;

procedure TModelContact.SetPHONE_NUMBER(const Value: String);
begin
  FPHONE_NUMBER := Value;
end;

end.
