unit Model.Question;

interface

uses
  System.SysUtils, Data.DB, Data.Connection, System.StrUtils;

type
  TModelQuestion = class
  private
    FNEXT_ID: Integer;
    FQUESTION: String;
    FMEDIAURL: String;
    FID: Integer;
    FCLEARDATA: String;
    FSTOREDATA: String;
    FACTION_TYPE: String;
    FINTENT: String;
    FENTITY: String;
    procedure SetCLEARDATA(const Value: String);
    procedure SetID(const Value: Integer);
    procedure SetMEDIAURL(const Value: String);
    procedure SetNEXT_ID(const Value: Integer);
    procedure SetQUESTION(const Value: String);
    procedure SetSTOREDATA(const Value: String);
    procedure SetACTION_TYPE(const Value: String);
    procedure SetENTITY(const Value: String);
    procedure SetINTENT(const Value: String);

    function DoFind(AConnection: TConnection): Boolean;
    function DoFindByIntentAndEntity(AConnection: TConnection): Boolean;
    procedure LoadProperties(ADataSet: TDataSet);
  public
    property ID: Integer read FID write SetID;
    property QUESTION: String read FQUESTION write SetQUESTION;
    property MEDIAURL: String read FMEDIAURL write SetMEDIAURL;
    property ACTION_TYPE: String read FACTION_TYPE write SetACTION_TYPE;
    property STOREDATA: String read FSTOREDATA write SetSTOREDATA;
    property CLEARDATA: String read FCLEARDATA write SetCLEARDATA;
    property NEXT_ID: Integer read FNEXT_ID write SetNEXT_ID;
    property INTENT: String read FINTENT write SetINTENT;
    property ENTITY: String read FENTITY write SetENTITY;

    class function Find(AConnection: TConnection; AID: Integer): TModelQuestion; overload;
    class function Find(AConnection: TConnection; AIntent, AEntity: String; out AID: Integer): Boolean; overload;
    class function Find(AConnection: TConnection; AID: Integer; var AText, AMediaURL, AActionType, ADataToStore: String;
      var AClearData: Boolean; out ANext: Integer): Boolean; overload;
  end;

implementation

{ TQuestion }

function TModelQuestion.DoFind(AConnection: TConnection): Boolean;
const
  FIND_BY_ID = 'SELECT * FROM QUESTION WHERE ID = %d';

var
  LDataSet: TDataSet;
begin
  Result := False;

  AConnection.FDConn.ExecSQL(Format(FIND_BY_ID, [Self.ID]), LDataSet);
  try
    if not LDataSet.IsEmpty then
    begin
      LoadProperties(LDataSet);
      Result := True;
    end;
  finally
    LDataSet.Free;
  end;
end;

class function TModelQuestion.Find(AConnection: TConnection; AID: Integer): TModelQuestion;
var
  LQuestion: TModelQuestion;
begin
  LQuestion := TModelQuestion.Create;

  LQuestion.ID := AID;
  LQuestion.DoFind(AConnection);

  Result := LQuestion;
end;

function TModelQuestion.DoFindByIntentAndEntity(AConnection: TConnection): Boolean;
const
  FIND_BY_INTENT_AND_ENTITY = 'SELECT * FROM QUESTION WHERE INTENT = ''%s'' AND COALESCE(ENTITY, '''') = ''%s''';

var
  LDataSet: TDataSet;
begin
  Result := False;

  AConnection.FDConn.ExecSQL(Format(FIND_BY_INTENT_AND_ENTITY, [Self.INTENT, Self.ENTITY]), LDataSet);
  try
    if not LDataSet.IsEmpty then
    begin
      LoadProperties(LDataSet);
      Result := True;
    end;
  finally
    LDataSet.Free;
  end;
end;

class function TModelQuestion.Find(AConnection: TConnection; AID: Integer; var AText, AMediaURL, AActionType,
  ADataToStore: String; var AClearData: Boolean; out ANext: Integer): Boolean;
var
  LQuestion: TModelQuestion;
begin
  Result := True;

  LQuestion := TModelQuestion.Find(AConnection, AID);
  try
    if not AText.Trim.IsEmpty then
    begin
      AText := AText + sLineBreak + sLineBreak;
      AText := AText + '------------------------------';
      AText := AText + sLineBreak + sLineBreak;
    end;

    AText := AText + LQuestion.QUESTION;

    if AMediaURL.Trim.IsEmpty then
    begin
      AMediaURL := LQuestion.MEDIAURL;
    end;

    if not LQuestion.STOREDATA.Trim.IsEmpty then
    begin
      ADataToStore := IfThen(not ADataToStore.Trim.IsEmpty, ADataToStore + '&') + LQuestion.STOREDATA;
    end;

    if not AClearData then
    begin
      AClearData := LQuestion.CLEARDATA = 'S';
    end;

    AActionType := LQuestion.ACTION_TYPE;
    ANext := LQuestion.ID;

    if LQuestion.NEXT_ID > 0 then
    begin
      TModelQuestion.Find(AConnection, LQuestion.NEXT_ID, AText, AMediaURL, AActionType, ADataToStore, AClearData, ANext);
    end;
  finally
    LQuestion.Free;
  end;
end;

class function TModelQuestion.Find(AConnection: TConnection; AIntent, AEntity: String; out AID: Integer): Boolean;
var
  LQuestion: TModelQuestion;
begin
  LQuestion := TModelQuestion.Create;
  try
    LQuestion.INTENT := AIntent;
    LQuestion.ENTITY := AEntity;

    if not LQuestion.DoFindByIntentAndEntity(AConnection) then
    begin
      Exit(False);
    end;

    AID := LQuestion.ID;
    Result := True;
  finally
    LQuestion.Free;
  end;
end;

procedure TModelQuestion.LoadProperties(ADataSet: TDataSet);
begin
  ID := ADataSet.FieldByName('ID').AsInteger;
  QUESTION := ADataSet.FieldByName('QUESTION2').AsString;
  MEDIAURL := ADataSet.FieldByName('MEDIAURL').AsString;
  STOREDATA := ADataSet.FieldByName('STOREDATA').AsString;
  CLEARDATA := ADataSet.FieldByName('CLEARDATA').AsString;
  NEXT_ID := ADataSet.FieldByName('NEXT_ID').AsInteger;
  ACTION_TYPE := ADataSet.FieldByName('ACTION_TYPE').AsString;
  INTENT := ADataSet.FieldByName('INTENT').AsString;
  ENTITY := ADataSet.FieldByName('ENTITY').AsString;
end;

procedure TModelQuestion.SetACTION_TYPE(const Value: String);
begin
  FACTION_TYPE := Value;
end;

procedure TModelQuestion.SetCLEARDATA(const Value: String);
begin
  FCLEARDATA := Value;
end;

procedure TModelQuestion.SetENTITY(const Value: String);
begin
  FENTITY := Value;
end;

procedure TModelQuestion.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TModelQuestion.SetINTENT(const Value: String);
begin
  FINTENT := Value;
end;

procedure TModelQuestion.SetMEDIAURL(const Value: String);
begin
  FMEDIAURL := Value;
end;

procedure TModelQuestion.SetNEXT_ID(const Value: Integer);
begin
  FNEXT_ID := Value;
end;

procedure TModelQuestion.SetQUESTION(const Value: String);
begin
  FQUESTION := Value;
end;

procedure TModelQuestion.SetSTOREDATA(const Value: String);
begin
  FSTOREDATA := Value;
end;

end.
