unit Data.Connection;

interface

uses
  System.SysUtils, System.Classes, VCL.Forms, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.IB,
  FireDAC.Phys.IBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, FireDAC.DApt, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef;

type
  TConnection = class(TDataModule)
    FDConn: TFDConnection;
    procedure FDConnBeforeConnect(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    class function GetConnection: TConnection;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TConnection }

procedure TConnection.FDConnBeforeConnect(Sender: TObject);
begin
  FDConn.Params.LoadFromFile(ExtractFilePath(Application.ExeName) + 'BD.INI');
end;

class function TConnection.GetConnection: TConnection;
begin
  Result := TConnection.Create(nil);
end;

end.
