object WebModule1: TWebModule1
  OldCreateOrder = False
  Actions = <
    item
      MethodType = mtPost
      Name = 'WebActionItem1'
      PathInfo = '/api/messages/receive'
      OnAction = WebModule1WebActionItem1Action
    end
    item
      MethodType = mtGet
      Name = 'WebActionItem2'
      PathInfo = '/api/teste'
      OnAction = WebModule1WebActionItem2Action
    end
    item
      MethodType = mtPost
      Name = 'WebActionItem3'
      PathInfo = '/api/messages/callback'
      OnAction = WebModule1WebActionItem3Action
    end>
  Height = 340
  Width = 550
end
