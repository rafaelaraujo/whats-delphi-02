﻿unit Controller.Message;

interface

uses
  System.SysUtils, System.StrUtils, Web.HTTPApp, Data.Connection, Provider.Message, Model.Contact, Model.Chat;

type
  TControllerMessage = class
  private
    FConnection: TConnection;

    procedure DoSendMessage(AChat: TModelChat; AFrom, ATo, ABody: String; AMediaURL: String = '');
    procedure DoCreateMessage(AMessage: TMessage; AChat: TModelChat);
    procedure DoSendQuestion(AChat: TModelChat; AQuestion: Integer; AFrom, ATo: String);
    procedure DontUnderstand(AMessage: TMessage; AChat: TModelChat);
    procedure DoSendMainMenuQuestion(AMessage: TMessage; AChat: TModelChat);
    procedure DoSaveContactName(AMessage: TMessage; AContact: TModelContact; AChat: TModelChat);
    procedure DoEndChat(AMessage: TMessage; AChat: TModelChat);

    function DoHealthCheck(AMessage: TMessage): Boolean;
    function DoAskNameIfNewContact(AMessage: TMessage; AChat: TModelChat; AContact: TModelContact; ANewContact: Boolean): Boolean;
    function DoSendMainMenuIfNewChat(AMessage: TMessage; AChat: TModelChat; ANewChat: Boolean): Boolean;
    function DoActionIfChatPreviouslyMarked(AMessage: TMessage; AContact: TModelContact; AChat: TModelChat): Boolean;
    function DoFindAnswerInQuestionList(AMessage: TMessage; AChat: TModelChat): Boolean;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Receive(ARequest: TWebRequest; AResponse: TWebResponse);
  end;

implementation

{ TControllerMessage }

uses Provider.Reader, Model.Message, Provider.Messenger, Model.Question, Model.Answer;

constructor TControllerMessage.Create;
begin
  inherited;

  FConnection := TConnection.GetConnection;
end;

destructor TControllerMessage.Destroy;
begin
  if Assigned(FConnection) then
  begin
    FConnection.Free;
  end;

  inherited;
end;

function TControllerMessage.DoActionIfChatPreviouslyMarked(AMessage: TMessage; AContact: TModelContact;
  AChat: TModelChat): Boolean;
begin
  case AnsiIndexStr(AChat.ACTION_TYPE, ['P', 'F', 'N', 'E']) of
    0: Exit(False);
    1: DoEndChat(AMessage, AChat);
    2: DoSaveContactName(AMessage, AContact, AChat);
    3: DoSendMessage(AChat, AMessage.&To, AMessage.From, AChat.GetStoredData('Email'));
  end;

  Result := True;
end;

function TControllerMessage.DoAskNameIfNewContact(AMessage: TMessage; AChat: TModelChat; AContact: TModelContact;
  ANewContact: Boolean): Boolean;
const
  QUESTION_WHO_ARE_YOU = 1;
begin
  if not ANewContact then
  begin
    Exit(False);
  end;

  DoSendQuestion(AChat, QUESTION_WHO_ARE_YOU, AMessage.&To, AMessage.From);
  Result := True;
end;

procedure TControllerMessage.DoCreateMessage(AMessage: TMessage; AChat: TModelChat);
begin
  TModelMessage.CreateNew(FConnection, AMessage.From, AMessage.&To, AMessage.Body, AChat.ID, AChat.QUESTION_ID);
end;

procedure TControllerMessage.DoEndChat(AMessage: TMessage; AChat: TModelChat);
begin
  AChat.FINISH := Now;
  AChat.Save(FConnection);

  DoSendMessage(AChat, AMessage.&To, AMessage.From, 'Até mais 😉👋');
end;

function TControllerMessage.DoFindAnswerInQuestionList(AMessage: TMessage; AChat: TModelChat): Boolean;
var
  LAnswer: TModelAnswer;
begin
  LAnswer := TModelAnswer.Find(FConnection, AChat.QUESTION_ID, AMessage.Body);
  try
    if LAnswer.ID = 0 then
    begin
      Exit(False);
    end;

    case AnsiIndexStr(LAnswer.ACTION_TYPE, ['P', 'F']) of
      0: DoSendQuestion(AChat, LAnswer.NEXTQUESTION_ID, AMessage.&To, AMessage.From);
      1: DoEndChat(AMessage, AChat);
    else Exit(False);
    end;

    Result := True;
  finally
    LAnswer.Free;
  end;
end;

function TControllerMessage.DoHealthCheck(AMessage: TMessage): Boolean;
const
  MESSAGE_RECEIVED = 'ping';
  MESSAGE_TO_SEND = 'pong';
begin
  if AMessage.Body.Trim.ToLower <> MESSAGE_RECEIVED then
  begin
    Exit(False);
  end;

  DoSendMessage(nil, AMessage.&To, AMessage.From, MESSAGE_TO_SEND);
  Result := True;
end;

procedure TControllerMessage.DontUnderstand(AMessage: TMessage; AChat: TModelChat);
const
  MESSAGE_WHAT = '🙄🙄' + sLineBreak + sLineBreak +
                 'Sou um robô que está sendo treinado. 🤷‍♂️' + sLineBreak +
                 'Por favor, digite o número da opção desejada ou uma frase simples.';

  ARRAY_WHAT: Array [0 .. 9] of String =
    ('https://gitlab.com/rafaelaraujo/integrando-sua-aplicacao-ao-whatsapp/raw/master/what_01.png',
     'https://gitlab.com/rafaelaraujo/integrando-sua-aplicacao-ao-whatsapp/raw/master/what_02.png',
     'https://gitlab.com/rafaelaraujo/integrando-sua-aplicacao-ao-whatsapp/raw/master/what_03.png',
     'https://gitlab.com/rafaelaraujo/integrando-sua-aplicacao-ao-whatsapp/raw/master/what_04.png',
     'https://gitlab.com/rafaelaraujo/integrando-sua-aplicacao-ao-whatsapp/raw/master/what_05.png',
     'https://gitlab.com/rafaelaraujo/integrando-sua-aplicacao-ao-whatsapp/raw/master/what_06.png',
     'https://gitlab.com/rafaelaraujo/integrando-sua-aplicacao-ao-whatsapp/raw/master/what_07.png',
     'https://gitlab.com/rafaelaraujo/integrando-sua-aplicacao-ao-whatsapp/raw/master/what_08.png',
     'https://gitlab.com/rafaelaraujo/integrando-sua-aplicacao-ao-whatsapp/raw/master/what_09.png',
     'https://gitlab.com/rafaelaraujo/integrando-sua-aplicacao-ao-whatsapp/raw/master/what_10.png');
begin
  Randomize;

  DoSendMessage(AChat, AMessage.&To, AMessage.From, MESSAGE_WHAT, ARRAY_WHAT[Random(10)]);
end;

procedure TControllerMessage.DoSaveContactName(AMessage: TMessage; AContact: TModelContact; AChat: TModelChat);
const
  MESSAGE_INVAID_NAME = '🙄 Esse nome está um pouco curto...' + sLineBreak + sLineBreak +
                        'Por favor, digite seu nome completo.';
begin
  if AMessage.Body.Trim.Length < 4 then
  begin
    DoSendMessage(AChat, AMessage.&To, AMessage.From, MESSAGE_INVAID_NAME);
    Exit;
  end;

  AContact.NAME := AMessage.Body;
  AContact.Save(FConnection);

  DoSendMainMenuQuestion(AMessage, AChat);
end;

function TControllerMessage.DoSendMainMenuIfNewChat(AMessage: TMessage; AChat: TModelChat; ANewChat: Boolean): Boolean;
begin
  if not ANewChat then
  begin
    Exit(False);
  end;

  DoSendMainMenuQuestion(AMessage, AChat);

  Result := True;
end;

procedure TControllerMessage.DoSendMainMenuQuestion(AMessage: TMessage; AChat: TModelChat);
const
  QUESTION_MAIN_MENU = 2;
begin
  DoSendQuestion(AChat, QUESTION_MAIN_MENU, AMessage.&To, AMessage.From);
end;

procedure TControllerMessage.DoSendMessage(AChat: TModelChat; AFrom, ATo, ABody, AMediaURL: String);
{$REGION 'Twilio Tokens'}
const
  ACCOUNT_SID = 'YOUR_ACCOUNT_SID';
  AUTH_TOKEN  = 'YOUR_AUTH_TOKEN';
{$ENDREGION}
begin
  TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, AFrom, ATo, ABody, AMediaURL);

  if AChat <> nil then
  begin
    TModelMessage.CreateNew(FConnection, AFrom, ATo, ABody, AChat.ID, AChat.QUESTION_ID);
  end;
end;

procedure TControllerMessage.DoSendQuestion(AChat: TModelChat; AQuestion: Integer; AFrom, ATo: String);
var
  LText: String;
  LMediaURL: String;
  LActionType: String;
  LNextQuestion: Integer;
  LDataToStore: String;
  LClearData: Boolean;
begin
  LText := EmptyStr;
  LMediaURL := EmptyStr;
  LActionType := EmptyStr;
  LDataToStore := EmptyStr;
  LClearData := False;

  if TModelQuestion.Find(FConnection, AQuestion, LText, LMediaURL, LActionType, LDataToStore, LClearData, LNextQuestion) then
  begin
    AChat.SetQuestion(FConnection, LNextQuestion, LActionType, LDataToStore, LClearData);

    DoSendMessage(AChat, AFrom, ATo, LText, LMediaURL);
  end;
end;

procedure TControllerMessage.Receive(ARequest: TWebRequest; AResponse: TWebResponse);
const
  SUCCESS_STATUS_CODE = 200;
  SUCCESS_MESSAGE = 'OK';

var
  LMessage: TMessage;
  LContact: TModelContact;
  LContactCreated: Boolean;
  LChat: TModelChat;
  LChatCreated: Boolean;
begin
  AResponse.StatusCode := SUCCESS_STATUS_CODE;
  AResponse.Content := SUCCESS_MESSAGE;

  LMessage := TReader.StringToMessage(ARequest.Content);
  try
    {$REGION 'Health Check'}
    if DoHealthCheck(LMessage) then
      Exit;
    {$ENDREGION}

    LContact := TModelContact.FindOrCreate(FConnection, LMessage.From, LContactCreated);
    try
      LChat := TModelChat.FindOrCreate(FConnection, LContact.ID, LChatCreated);
      try
        DoCreateMessage(LMessage, LChat);

        {$REGION 'Novo Contato'}
        if DoAskNameIfNewContact(LMessage, LChat, LContact, LContactCreated) then
          Exit;
        {$ENDREGION}

        {$REGION 'Gravar Nome (Ações)'}
        if DoActionIfChatPreviouslyMarked(LMessage, LContact, LChat) then
          Exit;
        {$ENDREGION}

        {$REGION 'Nova Conversa'}
        if DoSendMainMenuIfNewChat(LMessage, LChat, LChatCreated) then
          Exit;
        {$ENDREGION}

        {$REGION 'Encontrar Resposta'}
        if DoFindAnswerInQuestionList(LMessage, LChat) then
          Exit;
        {$ENDREGION}

        {Implementar IA}

        DontUnderstand(LMessage, LChat);
      finally
        LChat.Free;
      end;
    finally
      LContact.Free;
    end;
  finally
    LMessage.Free;
  end;
end;

end.
