program client;

uses
  Vcl.Forms,
  view.main in 'src\views\view.main.pas' {frmViewMain},
  service.twilio in 'src\services\service.twilio.pas',
  controller.main in 'src\controllers\controller.main.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmViewMain, frmViewMain);
  Application.Run;
end.
