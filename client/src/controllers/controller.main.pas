unit controller.main;

interface

uses
  System.SysUtils, System.Classes;

type
  TControllerMain = class
  public
    class function SendMessage(ATo, ABody, AMediaURL: String): Boolean;
  end;

const
  ACCOUNT_SID = 'YOUR_ACCOUNT_SID';
  AUTH_TOKEN  = 'YOUR_AUTH_TOKEN';
  PHONE_NUMBER = '+14155238886';

implementation

{ TControllerMain }

uses service.twilio;

class function TControllerMain.SendMessage(ATo, ABody, AMediaURL: String): Boolean;
var
  LTwilioClient: TTwilioClient;
  LParams: TStrings;
begin
  LTwilioClient := TTwilioClient.Create(ACCOUNT_SID, AUTH_TOKEN);
  try
    LParams := TStringList.Create;
    try
      LParams.Add('From=whatsapp:' + PHONE_NUMBER);
      LParams.Add('To=whatsapp:' + ATo);
      LParams.Add('Body=' + ABody);

      if not AMediaURL.Trim.IsEmpty then
      begin
        LParams.Add('MediaUrl=' + AMediaURL);
      end;

      Result := LTwilioClient.Post('Messages', LParams).Success;
    finally
      LParams.Free;
    end;
  finally
    LTwilioClient.Free;
  end;
end;

end.
